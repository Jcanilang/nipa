
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const app = express();
let port = process.env.PORT||4010;

app.get('/',function(req,res){
	res.send('Hello world');
});

// online
// const connectionStr = "mongodb+srv://user:0ptional@firstcluster-iz6sr.mongodb.net/sts_db?retryWrites=true&w=majority";
let connectionStr = process.env.DATABASE_URL || "mongodb+srv://user:0ptional@firstcluster-iz6sr.mongodb.net/sts_db?retryWrites=true&w=majority";



mongoose.connect(connectionStr,{useCreateIndex:true,useNewUrlParser:true,useUnifiedTopology: true});

mongoose.connection.once('open',()=>{
	console.log('now connected to the mongodb server')
});

//set the limit to upload file
app.use(bodyParser.json({limit:"15mb"}));

//allow user to access the folder by serving the static data
app.use("/images/",express.static("images"));



const server = require("./queries/queries.js");


server.applyMiddleware({app,path:"/somewheretostay"});



//server initialization
app.listen(port,()=>{
	console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`);

});


