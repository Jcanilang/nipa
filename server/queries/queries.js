const {ApolloServer,gql} = require("apollo-server-express");
const {GraphQlDataTime} = require("graphql-iso-date");
const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const uuidv1 = require('uuid/v1');
const fs = require('fs'); 

const User = require("../models/User.js");
const Category = require("../models/Category.js");
const Hut = require("../models/Hut.js");
const HutStatus = require("../models/HutStatus.js");
const Booking = require("../models/Booking.js");
const Role = require("../models/Role.js");
const BookingStatus = require("../models/BookingStatus.js");

const saltRounds = 5;
const customScalarResolver={
	Date: GraphQlDataTime
}

const typeDefs = gql`
	scalar Date

	type UserType{
		id:ID
		firstname:String!
		lastname:String!
		telNo:String!
		address:String!
		email:String!
		username:String!
		password:String!
		roleId:String!
		image:String!
		isDeleted:Boolean!
		role:RoleType
	}

	type CategoryType{
		id:ID
		name:String!
	}

	type BookingStatusType{
		id:ID
		name:String!
	}

	type HutStatusType{
		id:ID
		name:String!
	}

	type HutType{
		id:ID
		name:String!
		description:String!
		price:Float!
		hutStatusId: String!
		categoryId:String!
		image:String!
		isDeleted:Boolean!
		category: CategoryType
		hutStatus: HutStatusType
	}

	type BookingType{
		id:ID
		inDate:Date
		outDate:Date
		hutId:String!
		userId:String!
		statusId:String!
		total:Float!
		isDeleted:Boolean!
		hut:HutType
		user:UserType
		status:BookingStatusType
	}

	type RoleType{
		id:ID
		name:String!
	}

	type Query{
		getUsers:[UserType]
		getCategories:[CategoryType]
		getBookings:[BookingType]
		getHuts:[HutType]
		getHutStatuses:[HutStatusType]
		getBookingStatuses:[BookingStatusType]
		getRoles:[RoleType]

		getUser(id:ID!) :UserType
		getRole(id:ID!) :RoleType
		getCategory(id:ID!) :CategoryType
		getBooking(userId:String!) : [BookingType]
		getHut(id:ID!) : HutType
		getHutStatus(id:ID!) :HutStatusType
		getBookingStatus(id:ID!) :BookingStatusType
	}

	type Mutation{

		createUser(
		firstname:String!
		lastname:String!
		telNo:String!
		address:String!
		email:String!
		username:String!
		password:String!
		roleId:String!
		image:String!
		):UserType

		createCategory(
		name:String!
		):CategoryType

		createHut(
		name:String!
		description:String!
		price:Float!
		hutStatusId: String!
		categoryId:String!
		image:String!
		):HutType

		createBooking(
		inDate:String
		outDate:String
		hutId:String!
		userId:String!
		statusId:String!
		total:Float!
		):BookingType
		
		createHutStatus(
		name:String!
		):HutStatusType

		createBookingStatus(
		name:String!
		):BookingStatusType	

		createRole(
		name:String!
		):RoleType

		updateUser(
		id:ID!
		firstname:String!
		lastname:String!
		telNo:String!
		address:String!
		email:String!
		username:String!
		password:String!
		roleId:String!
		image:String!
		):UserType

		updateCategory(
		id:ID!
		name:String!
		):CategoryType

		updateHut(
		id:ID!
		name:String!
		description:String!
		price:Float!
		hutStatusId: String!
		categoryId:String!
		image:String!
		):HutType

		updateBooking(
		id:ID!
		inDate:Date
		outDate:Date
		hutId:String!
		userId:String!
		statusId:String!
		total:Float!
		):BookingType

		updateRequest(
		id:String!
		statusId:String!
		):Boolean
		
		updateHutStatus(
		id:ID!
		name:String!
		):HutStatusType

		updateBookingStatus(
		id:ID!
		name:String!
		):BookingStatusType	

		updateRole(
		id:ID!
		name:String!
		):RoleType

		deleteUser(
		id:String!
		):Boolean

		deleteHut(
		id:String!
		):Boolean

		deleteBooking(
		id:String!
		):Boolean

		logInUser(username:String!, password:String!):UserType

	}



`;

const resolvers = {
	Query:{
		getUsers:()=>{
			return User.find({isDeleted:false})
		},
		getHuts:()=>{
			return Hut.find({isDeleted:false})
		},
		getHutStatuses:()=>{
			return HutStatus.find({})
		},
		getBookings:()=>{
			return Booking.find({isDeleted:false})
		},
		getBookingStatuses:()=>{
			return BookingStatus.find({})
		},
		getCategories:()=>{
			return Category.find({})
		},

		getRoles:()=>{
			return Role.find({})
		},

		getUser:(_,args)=>{
			return User.findById(args.id)
		},
		getRole:(_,args)=>{
			return Role.findById(args.id)
		},
		getHut:(_,args)=>{
			return Hut.findById(args.id)
		},
		getHutStatus:(_,args)=>{
			return HutStatus.findById(args.id)
		},
		getBooking:(_,args)=>{
			return Booking.find({userId:args.userId , isDeleted:false})
		},
		getBookingStatus:(_,args)=>{
			return BookingStatus.findById(args.id)
		},
		getCategory:(_,args)=>{
			return Category.findById(args.id)
		},


	},

	Mutation: {
		createUser:(_,args) =>{
			let imageString = args.image
			let imageBase = imageString.split(";base64,").pop();
			let imageLocation = "images/"+uuidv1()+".png";

			fs.writeFile(imageLocation,
			 			imageBase,
			 			{encoding:"base64"},
			 			err=>{
			 				console.log("uploading image error!")
			 			}
			 			)


			let newUser = User({
				firstname:args.firstname,
				lastname:args.lastname,
				telNo:args.telNo,
				address:args.address,
				email:args.email,
				username:args.username,
				password:bcrypt.hashSync(args.password,saltRounds),
				roleId:args.roleId,
				image:imageLocation,
				isDeleted:false
			})

			return newUser.save()
		},
		createCategory:(_,args)=>{
			let newCategory = Category({
				name:args.name
			})
			return newCategory.save()
		},
		createRole:(_,args)=>{
			let newRole = Role({
				name:args.name
			})
			return newRole.save()
		},

		createBooking:(_,args)=>{
			let newBooking = Booking({
				inDate:args.inDate,
				outDate:args.outDate,
				hutId:args.hutId,
				userId:args.userId,
				statusId:args.statusId,
				total:args.total,
				isDeleted:false
			})
			return newBooking.save()
		},
		createHut:(_,args)=>{
			let imageString = args.image
			let imageBase = imageString.split(";base64,").pop();
			let imageLocation = "images/"+uuidv1()+".png";

			fs.writeFile(imageLocation,
			 			imageBase,
			 			{encoding:"base64"},
			 			err=>{
			 				console.log("uploading image error!")
			 			}
			 			)

			let newHut= Hut({
				name:args.name,
				description:args.description,
				price:args.price,
				hutStatusId: args.hutStatusId,
				categoryId: args.categoryId,
				image:imageLocation,
				isDeleted:false	

			})

			return newHut.save()
		},
		createBookingStatus:(_,args)=>{
			let newBookingStatus= BookingStatus({
				name:args.name
			})
			return newBookingStatus.save()
		},
		createHutStatus:(_,args) =>{
			let newHutStatus = HutStatus({
				name:args.name
			})
			return newHutStatus.save()
		},

		updateUser:(_,args)=>{
			let imageString = args.image
			let imageBase = imageString.split(";base64,").pop();
			let imageLocation = "images/"+uuidv1()+".png";

			fs.writeFile(imageLocation,
			 			imageBase,
			 			{encoding:"base64"},
			 			err=>{
			 				console.log("uploading image error!")
			 			}
			 			)

			let condition = {_id:args.id}
			let update = {
				firstname:args.firstname,
				lastname:args.lastname,
				telNo:args.telNo,
				address:args.address,
				email:args.email,
				username:args.username,
				password:args.password,
				roleId:args.roleId,
				image:imageLocation,
				isDeleted:false	
			}
			return User.findOneAndUpdate(condition,update)
		},

		updateRole:(_,args)=>{
			let condition = {_id:args.id}
			let update = {
				name:args.name
			}
			return Role.findOneAndUpdate(condition,update)
		},

		updateCategory:(_,args)=>{
			let condition = {_id:args.id}
			let update ={
				name:args.name
			}
			return Category.findOneAndUpdate(condition,update)
		},

		updateBooking:(_,args)=>{
			let condition = {_id:args.id}
			let update = {
				inDate:args.inDate,
				outDate:args.outDate,
				hutId:args.hutId,
				userId:args.userId,
				statusId:args.statusId,
				total:args.total,
				isDeleted:false
			}
			return Booking.findOneAndUpdate(condition,update)
		},

		updateBookingStatus:(_,args)=>{
			let condition = {_id:args.id}
			let update={
				name:args.name
			}
			return BookingStatus.findOneAndUpdate(condition,update)
		},

		updateHut:(_,args)=>{
			let imageString = args.image
			let imageBase = imageString.split(";base64,").pop();
			let imageLocation = "images/"+uuidv1()+".png";

			fs.writeFile(imageLocation,
			 			imageBase,
			 			{encoding:"base64"},
			 			err=>{
			 				console.log("uploading image error!")
			 			}
			 			)
			let condition = {_id:args.id}
			let update={
				name:args.name,
				description:args.description,
				price:args.price,
				hutStatusId: args.hutStatusId,
				categoryId: args.categoryId,
				image:imageLocation,
				isDeleted:false	
			}
			return Hut.findOneAndUpdate(condition,update)
		},
		updateHutStatus:(_,args)=>{
			let condition = {_id:args.id}
			let update={
				name:args.name
			}
			return HutStatus.findOneAndUpdate(condition,update)
		},
		deleteUser:(_,args)=>{
			let id = mongoose.Types.ObjectId(args.id);
			let condition = {_id:id}
			let update={
				isDeleted:true
			}
			return User.findOneAndUpdate(condition,update).then((user,err)=>{
				if(err ||!user){
					console.log("Delete failed");
					return false;
				}else{
					console.log("User Deleted")
					return true;
				}

			})
		},
		deleteHut:(_,args)=>{
			let id = mongoose.Types.ObjectId(args.id);
			let condition = {_id:id}
			let update={
				isDeleted:true
			}
			return Hut.findOneAndUpdate(condition,update).then((hut,err)=>{
				if(err ||!hut){
					console.log("Delete failed");
					return false;
				}else{
					console.log("Hut Deleted")
					return true;
				}

			})


		},
		deleteBooking:(_,args)=>{
			let id = mongoose.Types.ObjectId(args.id);
			let condition = {_id:id}
			let update={
				isDeleted:true
			}
			return Booking.findOneAndUpdate(condition,update).then((book,err)=>{
				if(err ||!book){
					console.log("Delete failed");
					return false;
				}else{
					console.log("Booking request Deleted")
					return true;
				}

			})


		},

		updateRequest:(_,args)=>{
			let id = mongoose.Types.ObjectId(args.id);
			let statusId = args.statusId;
			let condition = {_id:id}
			let update={
				statusId:statusId
			}
			return Booking.findOneAndUpdate(condition,update).then((booking,err)=>{
				if(err ||!booking){
					console.log("Request failed");
					return false;
				}else{
					console.log("Request update")
					return true;
				}

			})


		},
		logInUser:(_,args)=>{
			return User.findOne({
				username:args.username,
				isDeleted:false
			}).then(user =>{
				if(user === null){
					return null;
				}
				
				let hashedPass = user.password;
				let plainPass = args.password;

				const hashedPassword = bcrypt.compareSync(plainPass,hashedPass);

				if(!hashedPassword){
					console.log("Check your credentials");
					return null;
				}
				else{
					console.log("Login Success");
					return user;
				}

			});
		}


	},

	HutType:{
		category:(parent,args)=>{
			return Category.findOne({_id:parent.categoryId})
		},
		hutStatus:(parent,args)=>{
			return HutStatus.findOne({_id:parent.hutStatusId})
		}
	},

	BookingType:{
		hut:(parent,args)=>{
			return Hut.findOne({_id:parent.hutId})
			
		},
		user:(parent,args)=>{
			return User.findOne({_id:parent.userId})
		},
		status:(parent,args)=>{
			return BookingStatus.findOne({_id:parent.statusId})
		}
	},
	UserType:{
		role:(parent,args)=>{
			return Role.findOne({_id:parent.roleId})
		}
	}

}

const server = new ApolloServer({
	typeDefs,
	resolvers
});

module.exports =server;