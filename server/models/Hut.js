const mongoose = require("mongoose")
const Schema = mongoose.Schema

const hutSchema = new Schema({
	name:{
		type:String,
		required:true
	},
	description:{
		type:String,
		required:true
	},
	price:{
		type:Number,
		required:true
	},
	hutStatusId:{
		type:String,
		required:true
	},
	categoryId:{
		type:String,
		required:true
	},
	image:{
		type:String
	},
	isDeleted:{
		type:Boolean
	}
},{
	timestamps:true
})

 // export the model as a module
 module.exports = mongoose.model('Hut', hutSchema);