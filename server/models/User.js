const mongoose = require("mongoose")
const Schema = mongoose.Schema

const userSchema = new Schema({
	firstname:{
		type:String,
		required:true
	},
	lastname:{
		type:String,
		required:true
	},
	telNo:{
		type:String,
		required:true
	},
	address:{
		type:String,
		required:true
	},
	email:{
		type:String,
		required:true
	},
	username:{
		type:String,
		required:true
	},
	password:{
		type:String,
		required:true
	},
	roleId:{
		type:String,
		required:true
	},
	image:{
		type:String
	},
	isDeleted:{
		type:Boolean
	}

},{
	timestamps:true
})

 // export the model as a module
 module.exports = mongoose.model('User', userSchema);