const mongoose = require("mongoose")
const Schema = mongoose.Schema

const bookingStatusSchema = new Schema({
	name:{
		type:String,
		required:true
	}
},{
	timestamps:true
})

 // export the model as a module
 module.exports = mongoose.model('BookingStatus', bookingStatusSchema);