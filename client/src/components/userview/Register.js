import React, {useState} from "react";
import { Card, Form, Icon, Input, Button, Checkbox, Row, Col,message, Drawer, Select, DatePicker, Modal,Upload } from 'antd';
import { Link,Redirect } from "react-router-dom";
import { graphql } from "react-apollo";
import "../../App.css"

import {flowRight as compose} from "lodash";
import {getUsersQuery,getRolesQuery} from "../../queries/queries";
import { createUserMutation} from "../../queries/mutations";
const { Option } = Select;

const Register = (props) => {


      // const mongoose = require('mongoose');
  const dataUsers = props.getUsersQuery;

  const userData = dataUsers.getUsers ? dataUsers.getUsers:[];

  // console.log(UsersData);
  // hooks
  const [id, setId] = useState("");
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [address, setAddress] = useState("");
    const [telno, setTelno] = useState("");
    const [email,setEmail] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [roleId, setRoleId] = useState("");

    // let role = props.getRolesQuery.getRoles.name === "user" ? return{} props.getRolesQuery.getRoles.id :{}
    // console.log(props)
    
  // useEffect(()=>{
  //   // console.log(id);
  //   //     console.log(firstname);
  //   //     console.log(lastname);
  //   // console.log(address);
  //   // console.log(telno);
  //   // console.log(email);
  //   //     console.log(username);
  //   //     console.log(password);
  //   //     console.log(roleId);
  // });

  const firstnameChangeHandler = e =>{
    setFirstname(e.target.value);
    props.getRolesQuery.getRoles.map(role=>{
       if(role.name === "user"){
        setRoleId(role.id)
      }
    })
   
    
    }

  const lastnameChangeHandler = e =>{
    setLastname(e.target.value);
    // console.log(roleId)
    }

  const telnoChangeHandler = e =>{
    setTelno(e.target.value);
  }

  const addressChangeHandler = e =>{
    setAddress(e.target.value);
  }

  const emailChangeHandler = e =>{
    setEmail(e.target.value);
     userData.map(user =>{
      if(e.target.value=== user.email){
        message.error('Email has already taken!', 1.5)
      }
      else{
        setEmail(e.target.value);
      }
    })
  }

  const usernameChangeHandler = e =>{
    setUsername(e.target.value);
    userData.map(user =>{
      if(e.target.value=== user.username){
        message.error('Username has already taken!', 1.5)
      }
      else{
        setUsername(e.target.value);
      }
    })
  }

  const passwordChangeHandler = e =>{
        setPassword(e.target.value);
        // console.log(e)
    }



  // console.log()
  const addUser = e =>{

    e.preventDefault();
    if(imageUrl == null || firstname == "" || lastname== "" || address== "" || telno== "" || email== "" || username== "" || password== "" || roleId== ""){
      // imagePath = nodeServer()+"/images/b857ab90-1b48-11ea-8070-a3e900f687b3.png";
      message.error("Please fill up all the field!!");
    }
    else{
    let newUser = {   
        firstname:firstname,
        lastname:lastname,
        address:address,
        telNo:telno,
        email:email,
        username:username,
        password:password,
        roleId:roleId,
        image: imageUrl
    }

        let secondsToGo = 2;
        const modal = Modal.success({
            title: 'New User Added',
            content: `This modal will be destroyed after ${secondsToGo} second.`,
            render: window.location.href = "/login#huts"
        });
        const timer = setInterval(() => {
            secondsToGo -= 1;
            modal.update({
            content: `This modal will be destroyed after ${secondsToGo} second.`,
            render: window.location.href = "/login#huts"
            });
        }, 1000);
        setTimeout(() => {
            clearInterval(timer);
            modal.destroy();
        }, secondsToGo * 1000);


    props.createUserMutation({
      variables:newUser
    });

        console.log("success");
        setFirstname("")
        setLastname("")
        setAddress("")
        setTelno("")
        setEmail("")
        setUsername("")
        setPassword("")
        setImageUrl("")

        }
  }
 // uploading image
    const [loading, setLoading] = useState(false);
    const [imageUrl,setImageUrl] = useState();

    function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
      }


      function beforeUpload(file) {
          const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
          if (!isJpgOrPng) {
            message.error('You can only upload JPG/PNG file!');
          }
          const isLt2M = file.size / 1024 / 1024 < 2;
          if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
          }
          return isJpgOrPng && isLt2M;
        }


  const handleChange = info => {
    if (info.file.status === 'uploading') {
      setLoading(true)
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>{
          setImageUrl(imageUrl)
          setLoading(false)
      }
      );
    }


  };

    const uploadButton = (
      <div>
        <Icon type={loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );





	return(
		<div>
	    <Card>
	    	<Form layout="vertical" onSubmit={addUser}>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="First Name">
          <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="First name"  onChange={firstnameChangeHandler} value={firstname}/>
        </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Last Name">
          <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Last name"  onChange={lastnameChangeHandler} value={lastname}/>
        </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
               <Form.Item label="E-mail">
          <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="E-mal" htmlType="email" onChange={emailChangeHandler} value={email}/>
        </Form.Item>
              </Col>
              <Col span={12}>
        <Form.Item label="Tel No">
            <Input prefix={<Icon type="phone" style={{ color: 'rgba(0,0,0,.25)' }} />}
          placeholder="Tel. No." onChange={telnoChangeHandler} value={telno}/>
        </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
        <Form.Item label="User Name">
          <Input 
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Username" 
              onChange={usernameChangeHandler} 
              value={username}
              />
        </Form.Item>
              </Col>
              <Col span={12}>
        <Form.Item label="Password">
          <Input 
          prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
          placeholder="Password"
          type="password" 
          onChange={passwordChangeHandler} 
          value={password}
          />
        </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item label="Address">
                 <Input.TextArea prefix={<Icon type="environment" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Address" htmlType="email" rows={4} onChange={addressChangeHandler} value={address} />
                </Form.Item>
              </Col>
            </Row>
            <Form.Item>
            <Row gutter={16}>
              <Col span={24}>
             <Form.Item>
            <Upload
            name="avatar"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
            beforeUpload={beforeUpload}
            onChange={handleChange}
          >
            {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
          </Upload>
        </Form.Item>
              </Col>
            </Row>
          <Button type="primary"  htmlType="submit" block>
            Register
          </Button>
        </Form.Item>
          </Form>
	  </Card>
		</div>
		);
}

export default compose(
  graphql(getUsersQuery,{name:"getUsersQuery"}),
  graphql(getRolesQuery,{name:"getRolesQuery"}),
  graphql(createUserMutation,{name:"createUserMutation"}))(Register);
