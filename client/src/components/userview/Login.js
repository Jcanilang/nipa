import React, {useState} from "react";
import { Drawer, Card, Form, Icon, Input, Button, Checkbox, Row, Col,message } from 'antd';
import { Link,Redirect } from "react-router-dom";
import { graphql } from "react-apollo";
import "../../App.css"

import {flowRight as compose} from "lodash"
import {loginMutation} from "../../queries/mutations";

import Register from "./Register";

const Login = (props) => {
	const [username,setUsername] = useState("");
	const [password,setPassword] = useState("");
	const [logInSuccess, setLogInSuccess] = useState(false);

	const [visible, setVisible] = useState(false);

  const showDrawer = () => {
      setVisible(true)
  };

  const onClose = () => {
      setVisible(false)
  };



	const usernameChangeHandler = e => {
		setUsername(e.target.value);
	}

	const passwordChangeHandler = e => {
		setPassword(e.target.value);
	}

	const submitFormHandler = e =>{
		e.preventDefault();

		props.loginMutation({
			variables:{
				username:username,
				password:password
			}
		}).then(response =>{
			let data = response.data.logInUser;
			// console.log(data)
			if(data === null){
				message
				.loading('Action in progress..', 2.5)
    			.then(() => message.error('Login failed', 1.5))
			}
			else{
				message
				.loading('Action in progress..', 2.5)
    			.then(() => message.success('Login success', 1.5))
    			.then(()=> {
    				localStorage.setItem("role",data.role.name);
					localStorage.setItem("username",data.username);
					localStorage.setItem("id",data.id);
					// props.localStorageChecker();
					setLogInSuccess(true);
    			});
			}
		})

	}

	if(!logInSuccess){
				console.log("failed to login")
				// return <Redirect to="/login" />	
				}else{
				// return <Redirect to="/members" />
					if(localStorage.getItem("role") === "admin"){
						window.location.href ="/admin";
					}
					else{
						window.location.href ="/";
					}

				}


	return(
		<div>

		<Card
		className="login-App"
	    style={{ width: 300 }}
	    cover={
	      <img
	        alt="example"
	        src="./images/logo-img.png"
	      />
	    }
	  	>
	    <Card>
	    	<Form onSubmit={submitFormHandler} className="login-form">
	        	<Form.Item>
	         
	            <Input
	              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
	              placeholder="Username"
	              id="username"
	              onChange={usernameChangeHandler}
				  value ={username}

	            />
		        </Form.Item>
		        <Form.Item>
	            <Input
	              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
	              type="password"
	              placeholder="Password"
	              id="password"
	              onChange={passwordChangeHandler}
				  value ={password}
	            />
		        </Form.Item>
		        <Form.Item>
		          <Button type="primary" htmlType="submit" className="login-form-button">
		            Log in
		          </Button>
	           Or <a onClick={showDrawer}>register now!</a>
	        	</Form.Item>
      		</Form>
	    </Card>
	  </Card>

		<Drawer
          title="Create a new account"
          width={500}
          onClose={onClose}
          visible={visible}
          bodyStyle={{ paddingBottom: 80 }}
        >

        <Register />
        </Drawer>
		</div>
		);
}

export default compose(graphql(loginMutation,{name:'loginMutation'}))(Login);
