import React,{useState, useEffect} from "react";
import "../../App.css";
import {Link} from "react-router-dom";
import {graphql} from "react-apollo";
import moment from "moment";
import {flowRight as compose} from "lodash";
import {
    Icon,
    Row,
    Col,
    Button,
    Table, 
    Divider,
    Modal
  } from 'antd';

  import {getBookingQuery,getBookingStatusesQuery} from "../../queries/queries";
  import { updateRequestMutation } from "../../queries/mutations"; 

  const { Column, ColumnGroup } = Table;
  const { confirm } = Modal;

const Transaction = (props)=> {

      // const mongoose = require('mongoose');
	const dataBookings = props.getBookingQuery;
	const bookingsData = dataBookings.getBooking ? dataBookings.getBooking:[];
  const bookingStatus = props.getBookingStatusesQuery.getBookingStatuses?props.getBookingStatusesQuery.getBookingStatuses:[];

  const requestHandler = e =>{
    let id = e.target.id;
    let statusId = e.target.value;

    console.log(bookingsData)
    // console.log(e.target.value)

    confirm({
      title: 'Are you sure to cancel this request?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
               props.updateRequestMutation({
               variables: {
                  id:id,
                  statusId: statusId
                },
               refetchQueries:[{
                 query:getBookingQuery,
                 variables: { userId: localStorage.getItem("id") }
                 }]
              })

      }})
  }


    const columns = [
        {
          title: 'Code',
          key: 'key',
          dataIndex: 'key'

        },
        {
          title: 'First Name',
          key: 'firstname',
          dataIndex: 'firstname'

        },
        {
          title: 'Last Name',
          key: 'lastname',
          dataIndex: 'lastname'

        },
        {
          title: 'Hut Name',
          dataIndex: 'hutName',
          key: 'hutName'
        },
        {
          title: 'Price',
          key: 'price',
          dataIndex: 'price'

        },
        {
          title: 'In Date',
          dataIndex: 'inDate',
          key: 'inDate'
        },
        {
          title: 'Out Date',
          dataIndex: 'outDate',
          key: 'outDate'
        },
        {
          title: 'Status',
          key: 'status',
          dataIndex: 'status'

        },
        {
            title: 'Action',
            key: 'action',
            render: (data,record) => {


              // console.log(bookingStatus)
              let statId = ""

            	if(record.status ==="Pending"){ 

                    bookingStatus.map(bookStat=>{

                      if(bookStat.name === "Cancelled"){
                        statId = bookStat.id
                      }
                    
                    })
              
            		return(
	            		<span>
	                    <Button type="danger" onClick={requestHandler} value={statId} id={record.key} icon="check-circle" size="small" block  >Cancel</Button>
	                   </span>
            		);
            	}
            }         
            
          },
      ];

      const data = bookingsData.map(row=>({
      	key:row.id,
      	firstname:row.user.firstname,
      	lastname:row.user.lastname,
      	hutName:row.hut.name,
      	price:row.total,
      	inDate:moment(row.inDate).format('MM/DD/YYYY, h:mm a'),
      	outDate:moment(row.outDate).format('MM/DD/YYYY, h:mm a'),
      	status:row.status.name

      }))

      // console.log(data)

    return(
        <div>
        <h2>BOOKINGS PAGE</h2>
        <Row gutter={[16, 16]}>
      <Col span={24}>
        
      <Table columns={columns} dataSource={data} pagination={false}  scroll={{ y: 500 }}/>
      </Col>
    </Row>

        </div>
    );
};


export default compose(
  
  graphql(getBookingStatusesQuery,{name:"getBookingStatusesQuery"}),
  graphql(updateRequestMutation,{name:"updateRequestMutation"}),
  graphql(getBookingQuery,{name:"getBookingQuery",
  	options:props => {
    		return{
    			variables:{
    				userId:localStorage.getItem("id")
    			}
    		}
    	}

	})
	)(Transaction);