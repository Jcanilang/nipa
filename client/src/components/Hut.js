import React,{useState, useEffect} from "react";
import "../App.css";
import {Link} from "react-router-dom";
import {graphql} from "react-apollo";
import {flowRight as compose} from "lodash";
import {
    Form,
    Input,
    Icon,
    Cascader,
    Select,
    Row,
    Col,
    Checkbox,
    Button,
    Table, 
    Divider,
    Modal,
    Upload,
    message
  } from 'antd';
  import {toBase64, nodeServer} from "../function.js"

  import {getHutsQuery,getCategoriesQuery,getHutStatusesQuery} from "../queries/queries";
  import { createHutMutation,deleteHutMutation } from "../queries/mutations"; 
  import UpdateHut from "./UpdateHut";

  const { Option } = Select;
  const { Column, ColumnGroup } = Table;
  const { confirm } = Modal;

const Hut = (props)=> {
  // console.log(props)
      // const mongoose = require('mongoose');
	const dataHuts = props.getHutsQuery;
	const dataCategories = props.getCategoriesQuery;
	const dataHutStatuses = props.getHutStatusesQuery;

	const hutsData = dataHuts.getHuts ? dataHuts.getHuts:[];
	const categoriesData = dataCategories.getCategories ? dataCategories.getCategories:[];
	const hutStatusesData = dataHutStatuses.getHutStatuses ? dataHutStatuses.getHutStatuses:[];

	// console.log(hutsData);
	// hooks
	const [id, setId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [category, setCategory] = useState("");
	const [status, setStatus] = useState("");
  const [Id, setHutId] = useState();
  // const [image, setImage] = useState("");
  // const fileRef = React.createRef();


  const [modalVisible , setModalVisible] = useState(false);





	// useEffect(()=>{
	// 	console.log(id);
	// 	console.log(name);
	// 	console.log(description);
	// 	console.log(price);
	// 	console.log(category);
	// 	console.log(status);
	// });

	const nameChangeHandler = e =>{
		setName(e.target.value);
	}

	const descriptionChangeHandler = e =>{
		setDescription(e.target.value);
	}

	const priceChangeHandler = e =>{
		setPrice(e.target.value);
	}

	const categoryChangeHandler = e =>{
		setCategory(e);
	}

	const statusChangeHandler = e =>{
        setStatus(e);
        // console.log(e)
	}

  // const imageChangeHandler = e =>{
  //   toBase64(fileRef.current.files[0])
  //   .then(encodedFile => {
  //     // console.log(encodedFile);
  //     setImage(encodedFile);
  //   })


  // }


	


	const addHut = e =>{

		e.preventDefault();

    if(imageUrl == null || name == "" || description== "" || price== "" || status== "" || category== ""){
      // imagePath = nodeServer()+"/images/b857ab90-1b48-11ea-8070-a3e900f687b3.png";
      message.error("Please fill up all the field!!");
    }
    else{
      
		let newHut = {
			  name: name,
		    description: description,
		    price: parseFloat(price),
		    hutStatusId: status,
		    categoryId: category,
        image: imageUrl
    }
    
    let secondsToGo = 1;
    const modal = Modal.success({
        title: 'New Hut Added',
        content: `This modal will be destroyed after ${secondsToGo} second.`,
    });
    const timer = setInterval(() => {
        secondsToGo -= 1;
        modal.update({
        content: `This modal will be destroyed after ${secondsToGo} second.`,
        });
    }, 1000);
    setTimeout(() => {
        clearInterval(timer);
       
        modal.destroy();

    }, secondsToGo * 1000);

		   props.createHutMutation({
          variables:newHut,
          refetchQueries:[{
            query:getHutsQuery
          }]
        });

        console.log("success");
        
        setDescription("");
        setCategory("");
        setName("");
        setPrice("");
        setStatus("");
        setImageUrl("");
    }

	}

	const deleteHutHandler= e =>{
		let id = e.target.id;
    // console.log(id);
    confirm({
      title: 'Are you sure delete this hut?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
		props.deleteHutMutation({
			variables: {id:id},
			refetchQueries:[{
				query:getHutsQuery
				}]

    })
    console.log('OK');
              },
              onCancel() {
                console.log('Cancel');
              },
            });

	}

    const columns = [
        {
          title: 'Name',
          dataIndex: 'name',
          key: 'name'
        },
        {
          title: 'Price',
          dataIndex: 'price',
          key: 'price',
        },
        {
          title: 'Description',
          dataIndex: 'description',
          key: 'description',
        },
        {
          title: 'Category',
          key: 'category',
          dataIndex: 'category.name',

        },
        {
          title: '',
          dataIndex: 'image',
          render:  (data,record) => <img src={nodeServer()+record.image} className="img-user" />
         },
        {
            title: 'Action',
            key: 'action',
            render: (data,record) => (
                    <span>
                    <Button type="primary" size="large" id={record.key} icon="edit" onClick={modalHandler} />
                    <Divider type="vertical" />
                    <Button type="danger" size="large" icon="delete"  id={record.key} onClick={deleteHutHandler} />

                     <Modal
                      title="Update Hut"
                      style={{ top: 20 }}
                      visible={modalVisible}
                      onCancel={modalHandler}
                      okButtonProps={{ style: { display: 'none' } }}
                      cancelButtonProps={{ style: { display: 'none' } }}
                      
                      >
                      <UpdateHut id={Id} />
                    </Modal>
                  </span>

            )
          },
      ];

      const data = hutsData.map(row=>({
        key:row.id,
        name:row.name,
        price:row.price,
        description:row.description,
        category:row.category,
        image:row.image
      }))
      
      const modalHandler = (e) =>{
        let hutID = e.target.id;
        setModalVisible(!modalVisible);
        setHutId(hutID)
      }


  // uploading image
    const [loading, setLoading] = useState(false);
    const [imageUrl,setImageUrl] = useState();

    function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
      }


      function beforeUpload(file) {
          const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
          if (!isJpgOrPng) {
            message.error('You can only upload JPG/PNG file!');
          }
          const isLt2M = file.size / 1024 / 1024 < 2;
          if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
          }
          return isJpgOrPng && isLt2M;
        }


  const handleChange = info => {
    if (info.file.status === 'uploading') {
      setLoading(true)
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>{
          setImageUrl(imageUrl)
          setLoading(false)
      }
      );
    }


  };

    const uploadButton = (
      <div>
        <Icon type={loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );


    return(
        <div>
        <h2>HUTS PAGE</h2>
        <Row gutter={[16, 16]}>
      <Col span={6}>
        
      <Form onSubmit={addHut}>
        <Form.Item label="Hut Name">
          <Input onChange={nameChangeHandler} value={name}/>
        </Form.Item>
        
        <Form.Item label="Price">
          <Input type="Number" onChange={priceChangeHandler} value={price}/>
        </Form.Item>
        <Form.Item label="Status">
        <Select  onChange={statusChangeHandler} value={status}>
       
        {
		      	hutStatusesData.map(hutstatus=>{
		      		return(
		      			<Option value={hutstatus.id}>{hutstatus.name}</Option>
		      			);
		      	})
		      }
        </Select>
        </Form.Item>
        <Form.Item label="Category">
        <Select onChange={categoryChangeHandler} value={category} >
       
        {
		      	categoriesData.map(category=>{
		      		return(
          <Option value={category.id}>{category.name}</Option>
          );
		    })
		}
        </Select>
        </Form.Item>
        <Form.Item label="Description">
          <Input.TextArea rows={4} onChange={descriptionChangeHandler} value={description}  />
        </Form.Item>
        <Form.Item>
            <Upload
            name="avatar"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
            beforeUpload={beforeUpload}
            onChange={handleChange}
          >
            {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
          </Upload>
        </Form.Item>
        <Form.Item>
          <Button type="primary"  htmlType="submit" block>
            Add New
          </Button>
        </Form.Item>
      </Form>

      </Col>
      <Col span={18} >
      <Table columns={columns} dataSource={data} pagination={false}  scroll={{ y: 500 }}/>
      </Col>
    </Row>

        </div>
    );
};


export default compose(
	graphql(getHutsQuery,{name:"getHutsQuery"}),
	graphql(getCategoriesQuery,{name:"getCategoriesQuery"}),
	graphql(getHutStatusesQuery,{name:"getHutStatusesQuery"}),
	graphql(createHutMutation,{name:"createHutMutation"}),
	graphql(deleteHutMutation,{name:"deleteHutMutation"})

	)(Hut);