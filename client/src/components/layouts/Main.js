import React from "react";
import "../../App.css"

const main = (props)=> {
    return(
        <div className="App-main">
        {props.children}
        </div>
    );
};

export default main;

