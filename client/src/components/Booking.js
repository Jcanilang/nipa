import React,{useState, useEffect} from "react";
import "../App.css";
import {Link} from "react-router-dom";
import {graphql} from "react-apollo";
import moment from "moment";
import {flowRight as compose} from "lodash";
import {
    Icon,
    Row,
    Col,
    Button,
    Table, 
    Divider,
    Modal
  } from 'antd';

  import {getBookingsQuery,getBookingStatusesQuery} from "../queries/queries";
  import { updateRequestMutation,deleteBookingMutation } from "../queries/mutations"; 

  const { Column, ColumnGroup } = Table;
  const { confirm } = Modal;

const Booking = (props)=> {

      // const mongoose = require('mongoose');
	const dataBookings = props.getBookingsQuery;
	const bookingsData = dataBookings.getBookings ? dataBookings.getBookings:[];
  const bookingStatus = props.getBookingStatusesQuery.getBookingStatuses?props.getBookingStatusesQuery.getBookingStatuses:[];

  const requestHandler = e =>{
    let id = e.target.id;
    let statusId = e.target.value;

    // console.log(id)
    console.log(e.target.value)

    confirm({
      title: 'Are you sure to update this request?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
               props.updateRequestMutation({
               variables: {
                  id:id,
                  statusId: statusId
                },
               refetchQueries:[{
                 query:getBookingsQuery
                 }]

              })
      }})
   
  }

  const deleteBookingHandler = e =>{
    let id = e.target.id;
        // console.log(id);
            confirm({
              title: 'Are you sure delete this request?',
              okText: 'Yes',
              okType: 'danger',
              cancelText: 'No',
              onOk() {
                props.deleteBookingMutation({
                    variables: {id:id},
                    refetchQueries:[{
                        query:getBookingsQuery
                        }]
        
                })
                console.log('OK');
              },
              onCancel() {
                console.log('Cancel');
              },
            });

  }


    const columns = [
        {
          title: 'Code',
          key: 'key',
          dataIndex: 'key'

        },
        {
          title: 'First Name',
          key: 'firstname',
          dataIndex: 'firstname'

        },
        {
          title: 'Last Name',
          key: 'lastname',
          dataIndex: 'lastname'

        },
        {
          title: 'Hut Name',
          dataIndex: 'hutName',
          key: 'hutName'
        },
        {
          title: 'Price',
          key: 'price',
          dataIndex: 'price'

        },
        {
          title: 'In Date',
          dataIndex: 'inDate',
          key: 'inDate'
        },
        {
          title: 'Out Date',
          dataIndex: 'outDate',
          key: 'outDate'
        },
        {
          title: 'Status',
          key: 'status',
          dataIndex: 'status'

        },
        {
            title: 'Action',
            key: 'action',
            render: (data,record) => {


              // console.log(bookingStatus)
              let statId = ""
              let rejectId = ""

            	if(record.status ==="Pending"){ 

                    bookingStatus.map(bookStat=>{

                      if(bookStat.name === "Approved"){
                        statId = bookStat.id
                      }
                      else if(bookStat.name === "Rejected"){

                        rejectId = bookStat.id
                      
                      }
                    
                    })
              
            		return(
	            		<span>

	                    <Button type="primary" onClick={requestHandler} value={statId} id={record.key} icon="check-circle" size="small" block  >Approve</Button>
	                    <Button type="danger" icon="stop" onClick={requestHandler} value={rejectId} id={record.key} size="small" block >Reject</Button>
	                   
	                   </span>
            		);
            	}

            	if(record.status ==="Approved"){ 
                bookingStatus.map(bookStat=>{

                      if(bookStat.name === "Completed"){
                        statId = bookStat.id
                      }
                    
                    })

            		return(
	            		<span>
	                    <Button type="primary" size="small" onClick={requestHandler} value={statId} id={record.key} block icon="check-circle" >Complete</Button>
	                   </span>
            		);
            	}
            	if(record.status ==="Completed" || record.status ==="Cancelled" || record.status ==="Rejected"){ 
            		return(
	            		<span>
	                    <Button type="danger" size="small" onClick={deleteBookingHandler} id={record.key} block icon="delete" >Delete</Button>
	                   </span>
            		);
            	}

            }         
            
          },
      ];

      const data = bookingsData.map(row=>({
      	key:row.id,
      	firstname:row.user.firstname,
      	lastname:row.user.lastname,
      	hutName:row.hut.name,
      	price:row.total,
      	inDate:moment(row.inDate).format('MM/DD/YYYY, h:mm a'),
      	outDate:moment(row.outDate).format('MM/DD/YYYY, h:mm a'),
      	status:row.status.name

      }))

    return(
        <div>
        <h2>BOOKINGS PAGE</h2>
        <Row gutter={[16, 16]}>
      <Col span={20} offset={2}>
        
      <Table columns={columns} dataSource={data} pagination={false}  scroll={{ x:1100, y: 500 }}/>
      </Col>
    </Row>

        </div>
    );
};


export default compose(
  graphql(getBookingsQuery,{name:"getBookingsQuery"}),
  graphql(getBookingStatusesQuery,{name:"getBookingStatusesQuery"}),
  graphql(updateRequestMutation,{name:"updateRequestMutation"}),
	graphql(deleteBookingMutation,{name:"deleteBookingMutation"}),
	)(Booking);