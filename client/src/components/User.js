import React,{useState, useEffect} from "react";
import "../App.css";
import {Link} from "react-router-dom";
import {graphql} from "react-apollo";
import {flowRight as compose} from "lodash";
 import {toBase64, nodeServer} from "../function.js"
import {
    Form,
    Input,
    Icon,
    Cascader,
    Select,
    Row,
    Col,
    Checkbox,
    Button,
    Table, 
    Divider,
    Modal,
    message,
    Upload
  } from 'antd'; 


  import {getUsersQuery,getRolesQuery} from "../queries/queries";
  import { createUserMutation,deleteUserMutation } from "../queries/mutations"; 
  import UpdateUser from "./UpdateUser";

  const { Option } = Select;
  const { Column, ColumnGroup } = Table;
  const { confirm } = Modal;

const User = (props)=> {

      // const mongoose = require('mongoose');
	const dataUsers = props.getUsersQuery;
	const dataRoles = props.getRolesQuery;

	const userData = dataUsers.getUsers ? dataUsers.getUsers:[];
	const rolesData = dataRoles.getRoles ? dataRoles.getRoles:[];

	// console.log(UsersData);
	// hooks
	const [id, setId] = useState("");
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [address, setAddress] = useState("");
    const [telno, setTelno] = useState("");
    const [email,setEmail] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [roleId, setRoleId] = useState("");

     const [Id, setUserId] = useState();

    const [modalVisible , setModalVisible] = useState(false);


	// useEffect(()=>{
	// 	console.log(id);
 //        console.log(firstname);
 //        console.log(lastname);
	// 	console.log(address);
	// 	console.log(telno);
	// 	console.log(email);
 //        console.log(username);
 //        console.log(password);
 //        console.log(roleId);
	// });

	const firstnameChangeHandler = e =>{
		setFirstname(e.target.value);
    }
    const lastnameChangeHandler = e =>{
		setLastname(e.target.value);
    }
    const telnoChangeHandler = e =>{
		setTelno(e.target.value);
	}

	const addressChangeHandler = e =>{
		setAddress(e.target.value);
	}

	const emailChangeHandler = e =>{
		setEmail(e.target.value);
     userData.map(user =>{
      if(e.target.value=== user.email){
        message.error('Email has already taken!', 1.5)
      }
      else{
        setEmail(e.target.value);
      }
    })
	}

	const usernameChangeHandler = e =>{
    setUsername(e.target.value);
    userData.map(user =>{
      if(e.target.value=== user.username){
        message.error('Username has already taken!', 1.5)
      }
      else{
        setUsername(e.target.value);
      }
    })
	}

	const passwordChangeHandler = e =>{
        setPassword(e.target.value);
        // console.log(e)
    }
    const roleIdChangeHandler = e =>{
        setRoleId(e);
        // console.log(e)
	}


  // console.log()
	const addUser = e =>{

		e.preventDefault();

    // let imagePath = ""
    if(imageUrl == null || firstname == "" || lastname== "" || address== "" || telno== "" || email== "" || username== "" || password== "" || roleId== ""){
      // imagePath = nodeServer()+"/images/b857ab90-1b48-11ea-8070-a3e900f687b3.png";
      message.error("Please fill up all the field!!");
    }
    else{
      // imagePath=imageUrl;
      let newUser = {   
        firstname:firstname,
        lastname:lastname,
        address:address,
        telNo:telno,
         email:email,
        username:username,
        password:password,
        roleId:roleId,
        image: imageUrl
    }

        let secondsToGo = 1;
        const modal = Modal.success({
            title: 'New User Added',
            content: `This modal will be destroyed after ${secondsToGo} second.`,
        });
        const timer = setInterval(() => {
            secondsToGo -= 1;
            modal.update({
            content: `This modal will be destroyed after ${secondsToGo} second.`,
            });
        }, 1000);
        setTimeout(() => {
            clearInterval(timer);
            modal.destroy();
        }, secondsToGo * 1000);


    props.createUserMutation({
      variables:newUser,
      refetchQueries:[{
        query:getUsersQuery
      }]
    });

        console.log("success");
        setFirstname("")
        setLastname("")
        setAddress("")
        setTelno("")
        setEmail("")
        setUsername("")
        setPassword("")
        setRoleId("")
        setImageUrl("")
    }
	}

	const deleteUserHandler= e =>{
		let id = e.target.id;
        // console.log(id);
            confirm({
              title: 'Are you sure delete this user?',
              okText: 'Yes',
              okType: 'danger',
              cancelText: 'No',
              onOk() {
                props.deleteUserMutation({
                    variables: {id:id},
                    refetchQueries:[{
                        query:getUsersQuery
                        }]
        
                })
                console.log('OK');
              },
              onCancel() {
                console.log('Cancel');
              },
            });

	}

   const modalHandler = (e) =>{
        let userID = e.target.id;
        setModalVisible(!modalVisible);
        setUserId(userID);
      }

    const columns = [
        {
          title: '',
          dataIndex: 'image',
          render:  (usersData,record) => <img src={nodeServer()+record.image} className="img-user" />
         },
        {
          title: 'Firstname',
          dataIndex: 'firstname',
          key: 'firstname'
        },
        {
          title: 'Lastname',
          dataIndex: 'lastname',
          key: 'lastname',
        },
        {
          title: 'Address',
          dataIndex: 'address',
          key: 'address',
        },
        {
          title: 'Tel. No.',
          key: 'telNo',
          dataIndex: 'telNo',

        },
        {
            title: 'Email',
            key: 'email',
            dataIndex: 'email',
  
          },
          {
            title: 'User Name',
            key: 'username',
            dataIndex: 'username',
  
          },
          {
            title: 'Role',
            key: 'roleId',
            dataIndex: 'role.name',
  
          },
        {
            title: 'Action',
            key: 'action',
            render: (usersData,record) => (
                    <span>
                    <Button type="primary" size="large" id={record.id}  icon="edit" onClick={modalHandler} />
                    <Divider type="vertical" />
                    <Button type="danger" size="large" icon="delete"  id={record.id} onClick={deleteUserHandler} />
                  
                   <Modal
                      title="Update User"
                      style={{ top: 20 }}
                      visible={modalVisible}
                      onCancel={modalHandler}
                      okButtonProps={{ style: { display: 'none' } }}
                      cancelButtonProps={{ style: { display: 'none' } }}
                      
                      >
                      <UpdateUser id={Id} />
                    </Modal>

                  </span>
            )
          },
      ];
      

       // uploading image
    const [loading, setLoading] = useState(false);
    const [imageUrl,setImageUrl] = useState();

    function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
      }


      function beforeUpload(file) {
          const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
          if (!isJpgOrPng) {
            message.error('You can only upload JPG/PNG file!');
          }
          const isLt2M = file.size / 1024 / 1024 < 2;
          if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
          }
          return isJpgOrPng && isLt2M;
        }


  const handleChange = info => {
    if (info.file.status === 'uploading') {
      setLoading(true)
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>{
          setImageUrl(imageUrl)
          setLoading(false)
      }
      );
    }


  };

    const uploadButton = (
      <div>
        <Icon type={loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );





    return(
        <div>
        <h2>USERS PAGE</h2>
        <Row gutter={[16, 16]}>
      <Col sm={12} md={6}>
        
      <Form layout="vertical" onSubmit={addUser}>
        <Form.Item label="First Name">
          <Input onChange={firstnameChangeHandler} value={firstname}/>
        </Form.Item>
        <Form.Item label="Last Name">
          <Input onChange={lastnameChangeHandler} value={lastname}/>
        </Form.Item>
        <Form.Item label="Address">
          <Input onChange={addressChangeHandler} value={address}  />
        </Form.Item>
        <Form.Item label="Tel No">
          <Input onChange={telnoChangeHandler} value={telno}/>
        </Form.Item>
        <Form.Item label="E-mail">
          <Input onChange={emailChangeHandler} value={email}/>
        </Form.Item>
        <Form.Item label="User Name">
          <Input onChange={usernameChangeHandler} value={username}/>
        </Form.Item>
        <Form.Item label="Password">
          <Input type="password" onChange={passwordChangeHandler} value={password}/>
        </Form.Item>
        <Form.Item label="Role">
        <Select  onChange={roleIdChangeHandler} value={roleId}>
        {
		      	rolesData.map(role=>{
		      		return(
		      			<Option value={role.id}>{role.name}</Option>
		      			);
		      	})
		      }
        </Select>
        </Form.Item>
        <Form.Item>
            <Upload
            name="avatar"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
            beforeUpload={beforeUpload}
            onChange={handleChange}
          >
            {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
          </Upload>
        </Form.Item>
        <Form.Item>
          <Button type="primary"  htmlType="submit" block>
            Add New
          </Button>
        </Form.Item>
      </Form>

      </Col>
      <Col sm={12}  md={18} >
      <Table columns={columns} dataSource={userData} pagination={false}  scroll={{ y: 500 }}/>
      </Col>
    </Row>

        </div>
    );
};


export default compose(
	graphql(getUsersQuery,{name:"getUsersQuery"}),
	graphql(getRolesQuery,{name:"getRolesQuery"}),
	graphql(createUserMutation,{name:"createUserMutation"}),
	graphql(deleteUserMutation,{name:"deleteUserMutation"})

	)(User);